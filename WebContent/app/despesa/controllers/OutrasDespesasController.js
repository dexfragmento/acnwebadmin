/**
 * @author Filipe Gomes
 */

app.controller('OutrasDespesasController', ['$scope', 'AppService', function ($scope, AppService) {
	
	$scope.dto = {};
	$scope.listaFuncionarios = [];
	
	initController();
	
	$scope.limpar = function () {
		$scope.dto = {};
	};
	
	$scope.salvar = function () {
		$scope.$emit('load');
		$scope.dto.ativo = true;
		var service = AppService.salvar('/despesa/outrasDespesas', $scope.dto);		
		service.then(
				function (res) {
					$scope.limpar();
					console.info('Registro salvo!');
					$scope.$emit('unload');
					$scope.$emit('msg', {type: 'success', title: '', msg: 'Registro salvo com sucesso.'});
				},
				function (err) {
					console.error(err);
					$scope.$emit('unload');
					$scope.$emit('msg', {type: 'danger', title: '', msg: 'Falha ao salvar registro.'});
				});
	};
	
	$scope.deletar = function(id) {
		if (id !== undefined) {
			$scope.$emit('load');
			var service = AppService.deletar('/despesa', id);
			service.then(function(res) {
				$scope.listaEmpresas = res.data;
				console.info('Registro deletado!');
				$scope.$emit('unload');
				$scope.$emit('msg', {type: 'success', title: '', msg: 'Registro deletado com sucesso.'});
			}, function(err) {
				console.error(err);
				$scope.$emit('unload');
				$scope.$emit('msg', {type: 'danger', title: '', msg: 'Falha ao deletar registro.'});
			});
		}				
	};
	
	function initController () {
		carregarFuncionarios();
	};
	
	function carregarFuncionarios () {
		$scope.$emit('load');
		AppService.consultarTudo('/funcionario')
			.then(
					function (resultado) {
						$scope.listaFuncionarios = resultado.data;
						$scope.$emit('unload');
					},
					function (falha) {
						$scope.$emit('unload');
						$scope.$emit('msg', {type: 'danger', title: '', msg: 'Falha ao recuperar funcionários.'});
					});
	};
		
}]);