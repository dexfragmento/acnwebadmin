/**
 * @author Filipe Gomes
 */
app.directive('modalCadastroLocacao', function (AppService) {
	return {
		restrict: 'EA',
		templateUrl: 'app/locacao/views/cadastroModalLocacaoView.html',
		scope: {
			id: '=',
			listaLocacoes: '='
		},
		controller: function ($scope) {
			var service = AppService;
			$scope.dto = {};
			$scope.listaEmpresas = [];
			
			$scope.salvar = function () {
				$scope.$emit('load');
				$scope.dto.ativo = true;
				service.salvar('/locacao', $scope.dto)
					   .then(
							   function (res) {
								   if (res.data.id != null) {
									   $scope.listaLocacoes.push(res.data);
									   console.info('Registro salvo!');
									   $scope.limpar();
								   } else {
									   $scope.$emit('msg', {type: 'danger', title: '', msg: 'Falha ao salvar registro. Id null: Não persistido no banco!'});
								   }
								   
								   $scope.$emit('unload');
							   },
							   function (err) {
								   console.error(err);
								   $scope.$emit('unload');
							   });
			};
			
			$scope.limpar = function() {
				$scope.dto = {};
			};
			
			$scope.configurarCadastroEmpresa = function (id) {
				$scope.idCadastroEmpresa = id;
			};
			
			carregarEmpresas();
			
			function carregarEmpresas () {
				$scope.$emit('load');
				service.consultarTudo('/empresa')
					   .then(
						function (res) {
							$scope.listaEmpresas = res.data;
							$scope.$emit('unload');
						},
						function (err) {
							console.error(err);
							$scope.$emit('unload');
						});
			};
		}
	};
});