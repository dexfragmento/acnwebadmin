/**
 * @author Filipe Gomes
 */
app.directive('consultaUsuario', [function () {
	return {
		restrict: 'EA',
		templateUrl: 'app/usuario/views/consultaUsuarioView.html'
	}
}]);