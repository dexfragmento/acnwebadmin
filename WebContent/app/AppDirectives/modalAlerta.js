/**
 * @author Filipe Gomes
 */
app.directive('modalAlerta', function () {
	return {
		restrict: 'EA',
		templateUrl: 'app/AppViews/modalAlerta.html',
		scope: {
			id: '=',
			tituloModal: '=titulo',
			infoArray: '=array'
		}
	};
});