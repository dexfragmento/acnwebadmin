/**
 * @author Filipe Gomes
 */
app.controller('InfoContabilidadeController', [ '$scope', 'AppService', '$filter',
		function($scope, AppService, $filter) {
			$scope.dto = {};

			initController();

			$scope.salvar = function() {
				$scope.$emit('load');
			};

			$scope.limpar = function() {
				$scope.dto = {};
			};

			function initController() {
				/**
				 * Inicializacao da controller
				 */
				 carregarFuncionarios();
			};

			function carregarFuncionarios () {
				$scope.$emit('load');
				var service = AppService.consultarTudo('/funcionario');

				service
				 	.success(function (data) {
				 		$scope.listaFuncionarios = data;
				 		$scope.$emit('unload');
				 	})
				 	.error(function (err) {
				 		console.error(err);
				 		$scope.$emit('msg', {type: 'danger', title: '', msg: 'Falha ao carregar funcionarios.'});
				 	});
			};
		} ]);