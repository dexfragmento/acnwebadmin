/**
 * @author Filipe Gomes
 */
app.controller('ConsultaFuncionarioController', ['$scope', 'AppService', '$filter', function($scope, AppService, $filter){
	var service = AppService;
	
	$scope.dto = {};
	$scope.listaFuncionarios = [];
	
	initController();
				
	function initController () {
		carregarFuncionarios();
	};
	
	function carregarFuncionarios () {
		$scope.$emit('load');
		service.consultarTudo('/funcionario')
			.then(
					function (resultado) {
						$scope.listaFuncionarios = resultado.data;
						$scope.$emit('unload');
					},
					function (falha) {
						console.error(falha);
						$scope.$emit('msg', {type: 'danger', title: '', msg: 'Falha na requisição.'});
						$scope.$emit('unload');
					});
	};
	
}]);