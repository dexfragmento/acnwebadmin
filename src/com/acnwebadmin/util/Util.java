package com.acnwebadmin.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Util {

	public static Double arredondarValor(final Double valor) {
		final BigDecimal bd = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
		return bd.doubleValue();
	}
	
	public static Double aliquotaRecolhimentoINSS(final Double valor) {
		if(valor <= 1556.94){
			return valor * 0.08;
		} else if (valor <= 2594.92) {
			return valor * 0.09;
		} else {
			return valor * 0.11;
		}
	}
	
	public static void main(String args[]) {
		System.out.println(arredondarValor(0.544));
	}
	
}
