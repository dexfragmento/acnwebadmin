package com.acnwebadmin.rest;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.acnwebadmin.dao.DespesaDao;
import com.acnwebadmin.entity.OutrasDespesas;

@Path("/despesa")
public class DespesaRest {

	@POST
	@Path("/outrasDespesas")
	public Response setOutrasDespesas(final OutrasDespesas dto) {
		return Response.ok().entity(new DespesaDao().salvar(dto)).build();
	}
	
}
