package com.acnwebadmin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "INFO_CONTABILIDADE", schema = "ACN")
public class InfoContabilidade {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	@JoinColumn(name = "ID_FUNCIONARIO", nullable = false)
	private Funcionario funcionario;

	@Column(name = "MES", nullable = false)
	private Long mes;

	@Column(name = "SALARIO", nullable = false)
	private Double salario;

	@Column(name = "INSS", nullable = false)
	private Double inss;

	@Column(name = "FGTS", nullable = false)
	private Double fgts;

	@Column(name = "METADE_FGTS", nullable = false)
	private Double metadeFgts;

	@Column(name = "DECIMO_TERCEIRO", nullable = false)
	private Double decimoTerceiro;

	@Column(name = "FERIAS", nullable = false)
	private Double ferias;

	@Column(name = "TERCO_FERIAS", nullable = false)
	private Double terco_ferias;

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Long getMes() {
		return mes;
	}

	public void setMes(Long mes) {
		this.mes = mes;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Double getInss() {
		return inss;
	}

	public void setInss(Double inss) {
		this.inss = inss;
	}

	public Double getFgts() {
		return fgts;
	}

	public void setFgts(Double fgts) {
		this.fgts = fgts;
	}

	public Double getMetadeFgts() {
		return metadeFgts;
	}

	public void setMetadeFgts(Double metadeFgts) {
		this.metadeFgts = metadeFgts;
	}

	public Double getDecimoTerceiro() {
		return decimoTerceiro;
	}

	public void setDecimoTerceiro(Double decimoTerceiro) {
		this.decimoTerceiro = decimoTerceiro;
	}

	public Double getFerias() {
		return ferias;
	}

	public void setFerias(Double ferias) {
		this.ferias = ferias;
	}

	public Double getTerco_ferias() {
		return terco_ferias;
	}

	public void setTerco_ferias(Double terco_ferias) {
		this.terco_ferias = terco_ferias;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
