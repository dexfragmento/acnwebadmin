package com.acnwebadmin.dao;

import com.acnwebadmin.entity.OutrasDespesas;

public class DespesaDao extends GenericDao<Long, OutrasDespesas> {

	public DespesaDao() {
		super(OutrasDespesas.class);
	}
	
	public OutrasDespesas salvarOutrasDespesas(final OutrasDespesas dto) {
		return super.salvar(dto);
	}
	
}
