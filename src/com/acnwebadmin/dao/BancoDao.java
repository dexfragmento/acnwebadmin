package com.acnwebadmin.dao;

import com.acnwebadmin.entity.Banco;

public class BancoDao extends GenericDao<Long, Banco>{

	public BancoDao () {
		super(Banco.class);
	}
	
}
